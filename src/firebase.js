import firebase from 'firebase';

const config = {
    apiKey: "AIzaSyADFFFxAsTJj0WTLiPLqetFrRPIUQaLvdM",
    authDomain: "chating-1cadd.firebaseapp.com",
    databaseURL: "https://chating-1cadd-default-rtdb.firebaseio.com",
    projectId: "chating-1cadd",
    storageBucket: "chating-1cadd.appspot.com"
  };

  firebase.initializeApp(config);
  export default firebase;