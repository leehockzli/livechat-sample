import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import * as firebase from 'firebase'

import VueChatScroll from 'vue-chat-scroll'
Vue.use(VueChatScroll)

Vue.config.productionTip = false

new Vue({
  vuetify,
  router,
  store,
  render: function (h) { return h(App) },
  created(){
    firebase.initializeApp({
      apiKey: "AIzaSyADFFFxAsTJj0WTLiPLqetFrRPIUQaLvdM",
      authDomain: "chating-1cadd.firebaseapp.com",
      databaseURL: "https://chating-1cadd-default-rtdb.firebaseio.com",
      projectId: "chating-1cadd",
      storageBucket: "chating-1cadd.appspot.com",
    })
  },
}).$mount('#app')
